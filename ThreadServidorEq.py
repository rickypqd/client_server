#servidor
import socket
import threading
import math
'''
Integrantes 
Daniela Muñoz Pérez
Iván Darío Yépez
Juan David Martínez
Ricardo Caicedo
'''
host = ""
#host = "192.168.0.13"
port = 9798

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print ("Socket Created")
sock.bind((host, port))
print ("socket bind complete")
sock.listen(1)
print ("socket now listening")
'''
def eq(a,b,c):
    return (a+b+c)
'''
def cuad(a,b,c):
    d = (b**2)-4*a*c
    if d < 0:
        return "Ecuación No tiene soluciones reales"
    elif d == 0:
        return  -b/(2*a)
    else:
        x1 = (-b+math.sqrt(d))/(2*a)
        x2 = (-b-math.sqrt(d))/(2*a)
        return x1 , x2
def worker(*args):
    conn = args[0]
    addr = args[1]
    try:
        print('conexion con {}.'.format(addr))
        conn.send('Ecuacion Cuadratica'.encode('UTF-8'))
        #conn.send(' Pulse enter para iniciar...'.encode('UTF-8'))
        while True:
            datos = conn.recv(4096)
            if datos:
                conn.send(str("Respuesta : ").encode('UTF-8'))
                coef = datos.decode('utf-8')  # cast a string datos que envia el cliente
                coef_split = coef.split(",")
                a = int(coef_split[0])
                b = int(coef_split[1])
                c = int(coef_split[2])
                print("los coeficientes recibidos son: " )
                print(" A = {} , B = {} , C = {} ".format(a,b,c))
                print(" La solución de la ecuación es : ")
                print(cuad(a,b,c))
                conn.send(str(cuad(a,b,c)).encode('UTF-8'))
            else:
                print("prueba")
                break
    finally:
        print("close")
        conn.close()

while 1:
    conn, addr = sock.accept()
    threading.Thread(target=worker, args=(conn, addr)).start()
